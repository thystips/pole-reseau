# TP Pick What You Want

## Concept 

Ce document regroupe plusieurs sujets de TPs qui seront adaptés à vos envies. 
Choisissez un sujet et PONCEZ LE. 
Have Fun!

## Prérequis 

à minima :
 
* Un GNS3 fonctionnel
* iOS Cisco routeur
  * c'est avec un [c3745](https://drive.google.com/drive/folders/1hnOwFTcEYeznsBwjFCzKbDripnCLOJSQ)
  * ou un [c7200](https://drive.google.com/drive/folders/17F80VfaHEMKWLHRc9aHn60AmjizXeb-R) si votre PC tient la route
* [IOU L2 Cisco](https://www.gns3.com/marketplace/appliance/iou-l2)
  * si juste l'import de cette appliance ne marche pas, vous pouvez directement download le `.bin` [drive de téléchargement](https://drive.google.com/drive/folders/1LBIlztgGVAk4XsAeovKb1JHBrRg8QPSz).
* une machine CentOS7 prête à être clonée
  * pour tous les services qui tournent sous GNU/Linux

## Architecture réseau 

L'archi que vous pouvez utiliser : 
![Archireseau](./img/TPreseau.png)


**Tableau des réseaux utilisés**

Réseau | Adresse | VLAN | Description
--- | --- | --- | ---
`admin` | `10.2.10.0/24` | 10 | Admin
`web` | `10.2.20.0/24` | 20 | WEB
`remote` | `10.254.0.0/16` | x| IPsec
`users` | `10.2.30.0/24` | 30 | Utilisateurs

Réseau | Machines | 
--- | --- | 
`admin` | `PC1` | 
`web` | `SRV1 & SRV2` |
`remote` | `PC3` | 
`users` | `PC2` | 

Seuls les USERS ont besoin de DHCP. 
[IPhelpers](https://neptunet.fr/relais-dhcp/) nécéssaires 


**Serveurs**

* SRV1 : DNS + DHCP
* SRV2 : WEB

## _Sujets communs_ 

- :seedling: Dans ce réseau routé dynamiquement, lister et associer le rôle des routeurs. 
(Rappel des rôles [ici](https://gitlab.com/thystips/pole-reseau/-/blob/master/Cours/Approfondissement/1.OSPF.md))

- :seedling: Déployez cette topographie

HINTS : 
#### Conf DHCP

* serveur CentOS7
* installer le paquet `dhcp`
* configurer `/etc/dhcp/dhcpd.conf`
  * [exemple de conf](./hints/dhcpd.conf)
* démarrer le service `dhcpd`

Tester avec un client la récupération d'un IP et l'addresse de la passerelle, (et adresse de serveur DNS).

#### Conf DNS

* installer les paquets `bind` (serveur DNS) et `bind-utils` (outils DNS)
* configurer le serveur DNS en suivant les exemples de configuration
  * [`./hints/named.conf`](./hints/named.conf)
    * fichier de configuration de bind
  * [`./hints/dnsdomip.db`](./hints/dnsdomip.db)
    * fichier de zone
    * détermine quels noms de domaine seront traduisibles en IP
  * [`./hints/dnsipdom`](./hints/dnsipdom.db)
    * fichier de zone inverse
    * détermine quelles IPs pouront être traduites en noms de domaine
  * [exemples de conf](./dns)

Testez la résolution de noms :
* avec [dig](#nslookup-ou-dig)
* ou de simples `ping <NAME>` depuis les VPCS de GNS

#### Conf serveur web

* installer le paquet `epel-release` puis `nginx`
* démarrer le service `nginx`
* ouvrir le port `80/tcp`

Tester que les clients accèdent correctement au site Web.
* vous pouvez utiliser un client CentOS7

#### Router dynamiquement à travers un VPN

[Encapsulation dans tunnel GRE](https://cisco.goffinet.org/ccnp/vpn/lab-ipsec-esp-mode-tunnel-gre-ipsec-mode-transport-zbf/)

## _Sujets aux choix_ 

## 1. Security black & white hat

### A. Offensive 

#### ARP Spoofing

Usurpation d'identité L2/L3 : on usurpe l'adresse IP d'une machine du réseau en empoisonnant la table ARP de la victime.

Je recommande :
* [la commande `arping`](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)
* la librairie Python `scapy`
  * vous trouverez BEAUCOUP d'exemples sur internet

Le concept :
* l'attaquant envoie un ARP request (en broadcast donc)
  * il demande l'IP de la victime et précise
    * sa vraie MAC en source
    * une fausse IP en source
* la victime reçoit le message
  * la victime répond à l'attaquant avec un ARP reply (l'attaquant obtient l'adresse MAC de la victime)
  * **la victime inscrit dans sa table ARP les infos de la trame reçue** :
    * désormais la victime pense que l'IP envoyée (fausse) correspond à la MAC envoyée (celle de l'attaquant)
* répéter l'échange pour maintenir l'attaque en place
  * les entrées dans les tables ARP sont temporaires (de 60 à 120 secondes en général)

Pour mettre en place un man-in-the middle :
* l'attaquant usurpe l'IP de la victime auprès de la passerelle du réseau
* l'attaquant usurpe l'IP de la passerelle auprès de la victime
* tous les messages que la victime envoie vers d'autres reéseaux (internet par exemple) passent par l'attaquant

#### DHCP Spoofing

Mettre en place un rogue DHCP, aussi appelé "DHCP server of the doom" *(padutou)*.  
On met en place un serveur DHCP, et on file des IPs aux clients. On tire alors profit des *options DHCP* afin de fournir d'autres fausses infos aux clients. Principalement : adresse du serveur DNS du réseau, adresse de la passerelle.

On peut donc définir arbitrairement l'adresse de passerelle et celle du DNS pour les clients. Cela mène à des attaques Man-in-the-middle et DNS spoofing.

### B. Defensive

#### ARP Spoofing

Mise en place d'un *IP source guard*.

#### DHCP Spoofing

Mise en place de *DHCP snooping* qui permet d'interdire des trames DHCP sur les ports non-autorisés.

#### Messing up with VLANs

Mise en place de contre-mesures contre les attaques liées aux VLANs :
* désactiver explicitement les ports non utilisés
  * *a minima* désactiver la négociation
* le mécanisme *IP source guard* peut aussi aider

## 2. L'anonymat en ligne

## Proxy HTTP

Faites chauffer votre moteur de recherche préféré et cherchez un "free proxy https" pour tomber sur des sites [de ce genre](https://free-proxy-list.net/).

Configurez votre navigateur pour utiliser l'un de ces proxies HTTP et/ou HTTPS.

> Dans Firefox : Paramètres > Catégorie "Network Settings" > Bouton "Settings" > Manual configuration.

Une fois le proxy en place, essayez d'effectuer des requêtes. Proxy gratuit donc pas toujours OK, testez en un autre si ça passe pas.

🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant un proxy HTTP, puis un proxy HTTPS.
* vous pouvez aussi tester en visitant des sites en HTTP ou HTTPS si nécessaire pour voir la différence.

# Tor

Tor est un outil qui a pour but de proposer un réseau web alternatif. Ce réseau web est très similaire au web classique, mais inclut, au coeur du protocole Tor (The Onion Router), des fonctionnalités natives afin d'augmenter le niveau de confidentialité des échanges.  
Cela permet notamment aux utilisateurs du réseau d'avoir une garantie d'un anonymat (beaucoup) plus fort qu'avec l'utilisation du web classique.

## Connexion au web avec Tor

Téléchargez le [Tor Browser](https://www.torproject.org/download/).

Une fois en possesion du Tor Browser, lancez-le et faites un peu de navigation.  
Globalement, c'est pareil, juste plus lent. Ha et ptet pas dans la même langue. 

En cliquant sur le cadenas vert, à gauche de la barre d'URL, vous pouvez voir le circuit utilisé pour effectuer la connexion. 

Le protocole Tor utilise un chiffrement fort et trois noeuds intermédiaires par lesquels transitent les requêtes avant d'être émises.  
Les trois noeuds choisis par le client Tor sont appelés un *circuit Tor*. Il est choisi aléatoirement et c'est l'utilisation de circuit qui garantit l'anonymat des clients. 

L'encart d'info du cadenas vert fait référence au terme *Guard Node*.  
Le *Guard node*, ou *guard relay*, *entry relay* est le premier des trois noeuds intermédiaires. C'est celui auquel le client (vous) effectue la connexion.

🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant le Tor Browser, comparé à une connexion classique. 
* Testez avec un site en HTTP et HTTPS

## Hidden service Tor

Un *hidden service* c'est un service qui n'est pas disponible sur le web classique, mais uniquement en utilisant un navigateur compatible aec le protocole Tor.  
Ce dernier va nous permettre d'héberger des sites web classique au sein du réseau Tor, afin de renforcer grande l'anonymat du client comme du serveur.

Les échanges de clés sont nombreux lorsqu'un client se connecte à un hidden service, car le client crée toujours un circuit Tor, mais cette fois-ci, le serveur aussi.  
C'est donc un total de 6 noeuds intermédiaires. Plus exactement 7 noeuds, car le client et le serveur se rejoignent à un "point de rendez-vous" au sein du réseau Tor.

Il est très simple de créer un Hidden Service Tor. 

### Création d'un Hidden Service

Go sur une VM CentOS7.

```bash
# Mise à jour des dépôts
$ sudo yum install -y epel-release
$ sudo yum update -y 

# Installation d'un serveur web et de tor
$ sudo yum install -y nginx tor

# Modification de la page d'accueil NGINX pour l'identifier facilement
$ sudo vim /usr/share/nginx/html/index.html

# Démarrage du serveur NGINX
$ sudo systemctl enable --now nginx

# Modification de la configuration de tor
# Ajout d'un hidden service
$ sudo vim /etc/tor/torrc
# Dans /etc/tor/torrc, ajouter les lignes
HiddenServiceDir /var/lib/tor/hidden_service/
HiddenServicePort 80 127.0.0.1:80

# Création du répertoire de données pour tor
$ sudo mkdir /var/lib/tor/hidden_service/ -p
$ sudo chown -R toranon:toranon /var/lib/tor/hidden_service/
$ sudo chmod 700 -R /var/lib/tor/hidden_service/

# Démarrage et activation du hidden service
$ sudo systemctl restart tor
$ sudo systemctl enable tor

# Récupération de l'adresse du site
$ sudo cat /var/lib/tor/hidden_service/hostname
```

Une fois en possession de votre hostname en `.onion`, vous pouvez visiter votre hidden service avec le Tor Browser.

🌞 Lancez `tcpdump` sur la VM et récupérez la capture sur l'hôte afin de l'exploiter avec Wireshark :

```bash
$ sudo tcpdump -i <INTERFACE> -w <FILE>

# Par exemple
$ sudo tcpdump -i enp0s8 -w capture.pcap
```

> `.pcap` est l'extension standard pour les captures réalisées avec Wireshar ou `tcpdump`.

## 3. RADIUS et authentification par certificats 

Déployer un serveur RADIUS en 802.1x (protocole EAP-TLS)

Vous allez me déployer une petite archi avec des centos et un Windows Serveur 2019.

Pour cette archi il faudra, une authorité de certification qui va délivrer les certificats. 
Un serveur RADIUS.
Et des périphériques qui vont recevoir les certificats.

Après cette infra mise en place.

Essayez de connecter un périphérique extérieur a votre réseau et observez le comportement. 

Quelques ressources : 
- https://infradoc.antoinethys.com/infra/windows_server/comprendre_radius
- https://www.it-channels.com/microsoft/ad-cs-pki/119-installer-et-configurer-l-authentification-eap-tls

## 4. La seule limite en informatique, c'est l'imagination ! 

Si ces deux sujets ne correspondent pas à vos attentes, nous restons ouvert à vos propositions.