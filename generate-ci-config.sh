#!/bin/env bash
export ROOT=$(pwd);
for DOC in $(find . -name '*.tex' -printf "%P\n"); do
   DIR="$(dirname ${DOC})" ; FILE="$(basename ${DOC})" ; FILENAME="${FILE%.*}"
   cat <<EOF
job_${FILENAME}:
  stage: build
  image: thystips/texlive:latest
  script:
    - export TZ='Europe/Paris'
    - cd $DIR
    - latexmk -shell-escape -pdf -output-directory="${ROOT}/build" ${FILE}
  after_script:
    - cat build/${FILENAME}.log
  artifacts:
    paths:
      - build/${FILENAME}.pdf
  cache:
    paths:
      - build/

EOF
done